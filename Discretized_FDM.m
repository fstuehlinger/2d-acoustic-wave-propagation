function z = Discretized_FDM(A,t0,x0,x1,y0,y1,t,x,y,c)
    pd_x = ((2*x1)/(x0+x1) * A(t,A_c(x-x0),A_c(y)) -2*A(t,A_c(x),A_c(y)) + (2*x0)/(x0+x1) * A(t,A_c(x+x1),A_c(y)))/(x0*x1);
    pd_y = ((2*y1)/(y0+y1) * A(t,A_c(x),A_c(y-y0)) -2*A(t,A_c(x),A_c(y)) + (2*y0)/(y0+y1) * A(t,A_c(x),A_c(y+y1)))/(y0*y1);
    z = t0^2*(pd_x+pd_y)/((1/c^2)-1) + 2*A(t,A_c(x),A_c(y)) - A(t-1,A_c(x),A_c(y));
 end