function P = FDM_ABS(center, Left, Right, Bottom, Top, Boundary, h, dt, c)
%UNTITLED Summary of this function goes here
%   Boundary: 1=Left, 2=Right, 3=Bottom, 4=Top
    gamma = 1;
    if (Boundary == 1)
        %P = center*((gamma/dt)-(c/h)) + Right*(c/h);
        P = center + ((c*dt)/(gamma*h))*(Right-center);
    end
    if (Boundary == 2)
        %P = center*((gamma/dt)-(c/h)) + Left*(c/h);
        P = center + ((c*dt)/(gamma*h))*(Left-center);
    end
    if (Boundary == 3)
        %P = center*((gamma/dt)-(c/h)) + Top*(c/h);
        P = center + ((c*dt)/(gamma*h))*(Top-center);
    end
    if (Boundary == 4)
        %P = center*((gamma/dt)-(c/h)) + Bottom*(c/h);
        P = center + ((c*dt)/(gamma*h))*(Bottom-center);
    end
end

