function z = pIn(t,x,y,d,Amp,k,w)
    z = Amp*cos(sqrt(x*x+(y-d)*(y-d))*k + t*w);
end

