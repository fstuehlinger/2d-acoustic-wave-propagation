% grid definition
clear;

%d=1;
% Number of nodes in x and y direction
Nx=120;
Ny=114;
%h=d/Ny;
% Distance between two nodes
h = 0.1;

% For float comparisons
eps = 1e-7;

% Number of nodes in x direction in outer ear
NxOut = Nx/2;
% Number of nodes in y direction in outer ear
NyOut = Nx/2;
% Number of nodes in x direction in inner ear
NxIn = Nx/2;
% Number of nodes in y direction in inner ear
NyIn = Ny;

ind=0;
%ymax=Ny*h;
%d1=(ymax-h)/2.;

% Amplitude 1 for now
P0Amp=1.;

%m1=33;
%m2=38;
membraneIdx = NxOut + 1;

yOffset = ((NyIn-NyOut)/2) * h;

totalNumNodes = NxOut*NyOut + NxIn*NyIn;

% Preallocations
x = zeros(1, totalNumNodes);
y = zeros(1, totalNumNodes);
earPart = zeros(1, totalNumNodes);
type_bc = zeros(1, totalNumNodes);
value_bc = zeros(1, totalNumNodes);

for i=1:Nx
    
    %if(i>=m1 & i<m2)
        %ymax=ymax-h;
    %end
    
    % Case outer ear
    if (i < membraneIdx)
        for j=1:NyOut
            ind=ind+1;
            x(ind)=(i-1)*h;
            y(ind)=yOffset + (j-1)*h;
            
            % remember in which part of the ear the node is (1 -> outer, 2
            % -> on membrane, 3 -> inner)
            earPart(ind) = 1;

            % type of bc
            if(i==1 && j>1 && j<NyOut)
                type_bc(ind)=3; % ABC (input)
                %value_bc(ind)=P0Amp*cos((y(ind)-d1-h)*pi/(2*d1)); % value if we would want dirichlet conditions. 
                                                                  %How can this be time independent? 
            end
            if(j==1 || j==NyOut)
                type_bc(ind)=1; % ABC (regular)
                value_bc(ind)=0;
            end

            %if(y(ind)>=ymax)
                % if( i>1 & i<Nx)
                    %type_bc(ind)=1; % Dirichlet
                    %value_bc(ind)=0;
                % end
                %break;
            %end        
        end % end - j
    else
        % Case inner ear
        for j=1:NyIn
            ind=ind+1;
            x(ind)=(i-1)*h;
            y(ind)=(j-1)*h;

            % remember in which part of the ear the node is (1 -> outer, 2
            % -> on membrane, 3 -> inner)
            if (i == membraneIdx && j <= (NyOut + ((NyIn-NyOut)/2)) && j >= ((NyIn-NyOut)/2)+1)
                earPart(ind) = 2;
            else
                earPart(ind) = 3;
            end

            % type of bc
            if(j==1 || j==NyIn || (i == membraneIdx && (j > (NyOut + ((NyIn-NyOut)/2)) || j < (((NyIn-NyOut)/2)+1))) || i == Nx)
                type_bc(ind)=1; % ABC (regular)
                value_bc(ind)=0;
            end

            %if(y(ind)>=ymax)
                % if( i>1 & i<Nx)
                    %type_bc(ind)=1; % Dirichlet
                    %value_bc(ind)=0;
                % end
                %break;
            %end        
        end % end - j
    end
end % end - i

% definition of the cells
neighbors = zeros(ind, 4);
% 1 = Left, 2 = Right, 3 = Bottom, 4 = Top

for j=1:Ny % horizontal neighbors
    
    yc=(j-1)*h;
    ind_p=0;
    for i=1:ind
        if(y(i) >= yc-eps && y(i) <= yc+eps)
            if(ind_p>0)
                neighbors(i,1)=ind_p;
                neighbors(ind_p,2)=i;
            end
            ind_p=i;
        end
    end
end
for j=1:Nx % vertical neighbors
    xc=(j-1)*h;
    ind_p=0;
    for i=1:ind
        if(x(i) >= xc-eps && x(i) <= xc+eps)
            if(ind_p>0)
                neighbors(i,3)=ind_p;
                neighbors(ind_p,4)=i;
            end
            ind_p=i;
        end
    end
end

Ng=ind;
