clear;
grid_definition;
time = 10000;

dt = 0.1;

cIn = 0.05;
cOut = 0.01;
c = 0;

w = 0.01;

Amp = 0.5;

% Zero initial solution (Ng = number of grid nodes)
A = zeros(time, Nx, Ny);

inRight = zeros(1, NyIn);
inLeft = zeros(1, NyIn);
inUp = zeros(1, NxIn);
inLow = zeros(1, NxIn);
outLeft = zeros(1, NyOut);
outRight = zeros(1, NyOut);
outUp = zeros(1, NxOut);
outLow = zeros(1, NxOut);

inRightI = 1;
inLeftI = 1;
inUpI = 1;
inLowI = 1;
outLeftI = 1;
outRightI = 1;
outUpI = 1;
outLowI = 1;

for i = 1:NyOut
    u = x(i);
    v = y(i);
    %if(type_bc(i) == 3)
        A(1,A_c(u), A_c(v)) = Amp * cos(1*dt*w);
    %end
end

for t = 2:time-1
    for i = 1:Ng
       if (earPart(i) == 1)
           c = cOut;
       else
           c = cIn;
       end
       u = x(i);
       v = y(i);
       if(type_bc(i) == 1)
           %A(t+1,A_c(u), A_c(v)) = 0;
           % Outer ear
           if ((u <= ((NxOut-1) * h + eps)))
               % Left boundary (temporary as long as there is no inflow boundary)
               %  && v < NyOut * h + yOffset - eps && v > yOffset + eps
               if (u <= eps && u >= -eps && v < (NyOut-1) * h + yOffset - eps && v > yOffset + eps)
                   n = A(t, A_c(x(neighbors(i,2))), A_c(y(neighbors(i,2))));
                   A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),0,n,0,0,1,h,dt,c);
                   if (t == 2)
                    outLeft(outLeftI) = i;
                    outLeftI = outLeftI + 1;
                   end
               else
                    % Upper boundary
                    if (v <= (NyOut-1) * h + yOffset + eps && v >= (NyOut-1) * h + yOffset - eps)
                        n = A(t, A_c(x(neighbors(i,3))), A_c(y(neighbors(i,3))));
                        A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),0,0,n,0,4,h,dt,c);
                        if (t == 2)
                            outUp(outUpI) = i;
                            outUpI = outUpI + 1;
                        end
                    % Lower Boundary
                    else
                        n = A(t, A_c(x(neighbors(i,4))), A_c(y(neighbors(i,4))));
                        A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),0,0,0,n,3,h,dt,c);
                        if (t == 2)
                            outLow(outLowI) = i;
                            outLowI = outLowI + 1;
                        end
                    end
               end
          % Inner ear
           else
              % Right boundary
              if (u <= (Nx-1) * h + eps && u >= (Nx-1) * h - eps && v >= -eps && v <= (Ny-1) * h + eps)
                  n = A(t, A_c(x(neighbors(i,1))), A_c(y(neighbors(i,1))));
                  A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),n,0,0,0,2,h,dt,c);
                  if (t == 2)
                    inRight(inRightI) = i;
                    inRightI = inRightI + 1;
                  end
              else
                  % Lower boundary
                  if ((v >= (-eps)) && (v <= eps) && (u >= (NxOut * h - eps)) && (u < ((Nx-1) * h - eps)))
                      n = A(t, A_c(x(neighbors(i,4))), A_c(y(neighbors(i,4))));
                      A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),0,0,0,n,3,h,dt,c);
                      if (t == 2)
                        inLow(inLowI) = i;
                        inLowI = inLowI + 1;
                      end
                  else
                      % upper boundary
                      if (v >= (Ny-1) * h - eps && v <= (Ny-1) * h + eps && u >= NxOut * h - eps && u < (Nx-1) * h - eps)
                          n = A(t, A_c(x(neighbors(i,3))), A_c(y(neighbors(i,3))));
                          A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),0,0,n,0,4,h,dt,c);
                          if (t == 2)
                            inUp(inUpI) = i;
                            inUpI = inUpI + 1;
                          end
                      % Left boundary
                      else
                          n = A(t, A_c(x(neighbors(i,2))), A_c(y(neighbors(i,2))));
                          A(t+1,A_c(u), A_c(v)) = FDM_ABS(A(t,A_c(u),A_c(v)),0,n,0,0,1,h,dt,c);
                          if (t == 2)
                            inLeft(inLeftI) = i;
                            inLeftI = inLeftI + 1;
                          end
                      end
                  end
              end
                          
           end
       end
       if(type_bc(i) == 3)
           %A(t+1,A_c(u), A_c(v)) = p_In(t*dt, u, v, (Ny/2)*h, Amp, w*c, w);
           A(t+1, A_c(u), A_c(v)) = Amp * cos(w*t*dt);
           %this = A(t, A_c(u), A_c(v));
           %n = A(t, A_c(x(neighbors(i,2))), A_c(y(neighbors(i,2))));
           %A(t+1,A_c(u), A_c(v)) = ((cOut*dt)/(h)) * (n - this) + this - Amp*dt*sin(0.5*t);
           %A(t+1,A_c(u), A_c(v)) = ((cOut*dt)/(h)) * (n - this) + this + 2*Amp*(cos(w*(t+1)*dt) - cos(w*t*dt));
       end
       if(type_bc(i) == 0)
           A(t+1,A_c(u), A_c(v)) = Discretized_FDM(A,dt,(u-x(neighbors(i,1))),(x(neighbors(i,2))-u),(v-y(neighbors(i,3))), (y(neighbors(i,4))-v),t,u,v,c);
       end
    end
end

set(gca,"NextPlot","replacechildren")
v = VideoWriter("analyticInflow_dt=0.1_cIn=0.05_cOut=0.01_h=0.1_Nx=40_Ny=38_Amp=10.avi");
open(v);

for f=1:10:time
    imagesc(squeeze(A(f,:,:)));
    colorbar;
    caxis([-0.5, 0.5]);
    frame = getframe(gcf);
    writeVideo(v, frame);
    pause(0.00001);
    cla;
end

close(v);

% B = zeros(time, Nx, Ny);
% 
% for t=1:time
%     for x=1:Nx
%         for y=1:Ny
% %             if ((y < yOffset && x < (Nx/2 -1)*h) || (y > yOffset + NxOut*h && x < (Nx/2 -1)*h))
% %                 B(t,x,y) = 0;
% %             else
%                 %B(t,x,y) = p_In(t*dt, (x-1)*h, (y-1)*h, (Ny/2)*h, Amp, w/300, w);
%                 B(t,x,y) = Amp*cos(sqrt(x^2+(y-(Ny/2)*h)^2)*0.5*2*pi/100 + 0.5*2*pi*t);
%             %end
%         end
%     end
% end
% 
% for t=1:time
%     imagesc(squeeze(B(t, :, :)));
%     pause(0.1);
%     cla;
% end
